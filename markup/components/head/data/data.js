head: {
    index: {
        title: 'Головна',
        useSocialMetaTags: true
    },
    news: {
        title: 'Новини',
        useSocialMetaTags: true
    },
    investors: {
        title: 'Інвестори',
        useSocialMetaTags: true
    },
    registr: {
        title: 'Регістрація',
        useSocialMetaTags: true
    },
    projectDetails: {
        title: 'Опис проекта',
        useSocialMetaTags: true
    },
    map: {
        title: 'Інвестиційна карта',
        useSocialMetaTags: true
    },
    registrProj: {
        title: 'Регістрація проекту',
        useSocialMetaTags: true
    },
}

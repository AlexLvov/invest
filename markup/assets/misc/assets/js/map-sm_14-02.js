/*
 * 5 ways to customize the Google Maps infowindow
 * 2015 - en.marnoto.com
 * http://en.marnoto.com/2014/09/5-formas-de-personalizar-infowindow.html
*/

// map center
var center = new google.maps.LatLng(40.589500, -8.683542);

// marker position
var factory = new google.maps.LatLng(40.589500, -8.683542);

function initialize() {
  var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(50.4501, 30.5234), // Kiev

                    scrollwheel: false,


                    // How you would like to style the map.
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
  };

  var map = new google.maps.Map(document.getElementById("map"),mapOptions);

  // InfoWindow content
                var content = '<div id="iw-container" class="map-popover">' +
                              '<div class="popover-inner">' +
                                '<div class="popover-content iw-content">' +
                                  '<div class="map-balloon">' +
                                    '<div class="map-balloon-title media bg-primary">' +
                                      '<div class="media-left cell">' +
                                        '<img class="map-balloon-img" alt="Газоснабжение" src="assets/media/content/map-markers/gas/6.png">' +
                                      '</div>' +
                                      '<div class="media-body cell">' +
                                        '<a href="/contactcenter/request/2024" class="id link un">№ 2024</a><br>' +
                                        '<strong class="problem text-white">' +
                                          '<span class="text-elipsis">Газоснабжение:</span>' +
                                          '<span class="text-elipsis"> Ремонт системы газоснабжения</span>' +
                                        '</strong>' +
                                      '</div>' +
                                    '</div>' +
                                    '<div class="map-balloon-address text-elipsis">' +
                                      '<span class="fa fa-map-marker text-primary"></span>&nbsp;&nbsp;' +
                                      '<span>улица Леси Украинки 60, Дружковка</span>' +
                                    '</div>' +
                                    '<div class="map-balloon-info map-balloon-row">' +
                                      '<div class="map-balloon-col">' +
                                        '<div class="map-balloon-info-row">' +
                                          '<span class="name">Дата подачи обращения:</span>' +
                                          '<br>16.01.2017 09:38' +
                                        '</div>' +
                                        '<div class="map-balloon-info-row">' +
                                          '<span class="name">Исполнитель:</span><br>' +
                                          '<span class="text-elipsis">Управління по газопостачанню та газифікації</span>' +
                                        '</div>' +
                                      '</div>' +
                                      '<div class="map-balloon-col">' +
                                        '<div class="map-balloon-info-row">' +
                                          '<span class="name">Плановая дата выполнения:</span>' +
                                          '<br>17.01.2017 09:38</div>' +
                                        '<div class="map-balloon-info-row">' +
                                          '<span class="name">Статус:</span><br>' +
                                          '<span class="text-primary">Выполнено</span>' +
                                        '</div>' +
                                      '</div>' +
                                    '</div>' +
                                    '<div class="map-balloon-row">' +
                                      '<div class="map-balloon-info-row text-center">' +
                                        '<span class="name">Дата закрытия: </span> 16.01.2017 09:57' +
                                      '</div>' +
                                    '</div>' +
                                  '</div>' +
                                '</div>' +
                              '</div>' +
                            '</div>';

  // A new Info Window is created and set content
  var infowindow = new google.maps.InfoWindow({
    content: content,

    // Assign a maximum value for the width of the infowindow allows
    // greater control over the various content elements
    maxWidth: 350
  });

  // marker options
  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(50.4501, 30.5234),
    map: map,
    icon: 'assets/media/content/map-markers/normal/marker-normal-01.png',
    draggable: true,
    animation: google.maps.Animation.DROP
  });

  // This event expects a click on a marker
  // When this event is fired the Info Window is opened.
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });

  // Event that closes the Info Window with a click on the map
  google.maps.event.addListener(map, 'click', function() {
    infowindow.close();
  });

  // *
  // START INFOWINDOW CUSTOMIZE.
  // The google.maps.event.addListener() event expects
  // the creation of the infowindow HTML structure 'domready'
  // and before the opening of the infowindow, defined styles are applied.
  // *
  google.maps.event.addListener(infowindow, 'domready', function() {

    // Reference to the DIV that wraps the bottom of infowindow
    var iwOuter = $('.gm-style-iw');

    /* Since this div is in a position prior to .gm-div style-iw.
     * We use jQuery and create a iwBackground variable,
     * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
    */
    var iwBackground = iwOuter.prev();

    // Removes background shadow DIV
    iwBackground.children(':nth-child(2)').css({'display' : 'none'});



    // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
    iwCloseBtn.mouseout(function(){
      $(this).css({opacity: '1'});
    });
  });
}
google.maps.event.addDomListener(window, 'load', initialize);

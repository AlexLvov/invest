'head': {
    index: {
        title: 'Головна',
        useSocialMetaTags: true
    },
    news: {
        title: 'Новини',
        useSocialMetaTags: true
    },
    investors: {
        title: 'Інвестори',
        useSocialMetaTags: true
    },
    registr: {
        title: 'Регістрація',
        useSocialMetaTags: true
    },
    projectDetails: {
        title: 'Опис проекта',
        useSocialMetaTags: true
    },
    map: {
        title: 'Інвестиційна карта',
        useSocialMetaTags: true
    },
    registrProj: {
        title: 'Регістрація проекту',
        useSocialMetaTags: true
    },
}
,

/* Module data structure */

// moduleName: {
//     dataType: {
//         property: value
//     }
// }

/* Module data example */

_template: {
    big: {
        title: 'Hello world',
        age: 10,
        button: false
    }
},

__iconsData: {
    
        'clock': {
            width: '35px',
            height: '35px'
        },
    
        'facebook': {
            width: '35px',
            height: '35px'
        },
    
        'icon-1': {
            width: '46px',
            height: '39px'
        },
    
        'icon-2': {
            width: '39px',
            height: '39px'
        },
    
        'icon-3': {
            width: '39px',
            height: '39px'
        },
    
        'icon-4': {
            width: '49px',
            height: '39px'
        },
    
        'icon-5': {
            width: '40px',
            height: '39px'
        },
    
        'icon-6': {
            width: '40px',
            height: '39px'
        },
    
        'icon-7': {
            width: '40px',
            height: '39px'
        },
    
        'letter': {
            width: '35px',
            height: '35px'
        },
    
        'phone': {
            width: '35px',
            height: '35px'
        },
    
        'place': {
            width: '35px',
            height: '35px'
        },
    
},

__pages: [{
                name: '0',
                href: '0.html'
             },{
                name: 'index-2',
                href: 'index-2.html'
             },{
                name: 'index-3',
                href: 'index-3.html'
             },{
                name: 'index-sm',
                href: 'index-sm.html'
             },{
                name: 'index',
                href: 'index.html'
             },{
                name: 'investors',
                href: 'investors.html'
             },{
                name: 'list-project',
                href: 'list-project.html'
             },{
                name: 'map',
                href: 'map.html'
             },{
                name: 'news-details',
                href: 'news-details.html'
             },{
                name: 'news',
                href: 'news.html'
             },{
                name: 'project-details-2',
                href: 'project-details-2.html'
             },{
                name: 'project-details',
                href: 'project-details.html'
             },{
                name: 'registration-investors',
                href: 'registration-investors.html'
             },{
                name: 'registration-project',
                href: 'registration-project.html'
             }]
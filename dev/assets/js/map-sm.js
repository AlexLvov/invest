
var map, infoBubble;
function init() {
  var mapCenter = new google.maps.LatLng(50.4501, 30.5234);
  map = new google.maps.Map(document.getElementById('map'), {
    center: mapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    // How zoomed in you want the map to start at (always required)
    zoom: 11,

    scrollwheel: false,


    // How you would like to style the map.
    // This is where you would paste any style found on Snazzy Maps.
    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
  });

  var marker = new google.maps.Marker({
    map: map,
    position: new google.maps.LatLng(50.4501, 30.5234),
    draggable: true
  });

  var contentString =
    '<a class="map-popover map-popover_without-img clearfix" href="#">' +

      '<span class="map-popover__inner">' +
        '<span class="map-popover__title">Довга-довга предовга назва проекту Довга-довга предовга назва проекту</span>' +
        '<span class="map-popover__title">Довга-довга предовга назва проекту</span>' +
        '<span class="map-popover__title">Довга-довга предовга назва проекту</span>' +
        '<span class="map-popover__title">Довга-довга предовга назва проекту</span>' +
      '</span>' +
    '</a>' ;

  // Это предыдущие окно
  // var contentString =
    // '<a class="map-popover clearfix" href="#">' +

    //   '<span class="map-popover__img">' +
    //     '<img class="img-responsive" src="assets/media/content/map-img/map-img_1.jpg" alt="foto">' +
    //   '</span>' +
    //   '<span class="map-popover__inner">' +
    //     '<span class="map-popover__title">Довга-довга предовга назва проекту</span>' +
    //     '<span class="map-popover__date">12.03.2017</span>' +
    //   '</span>' +
    // '</a>' ;

  infoBubble = new InfoBubble({
    maxWidth: 200,
    content: contentString
  });

  infoBubble.open(map, marker);

  var marker = new Marker({
    map: map,
    position: new google.maps.LatLng(50.4501, 30.5234),
    icon: {
      path: MAP_PIN,
      fillColor: '#00CCBB',
      fillOpacity: 1,
      strokeColor: '',
      strokeWeight: 0
    },
    map_icon_label: '<span class="map-icon map-icon-point-of-interest"></span>'
  });

  google.maps.event.addListener(marker, 'click', function() {
    if (!infoBubble.isOpen()) {
      infoBubble.open(map, marker);
    }
  });

  // Event that closes the Info Window with a click on the map
  google.maps.event.addListener(map, 'click', function() {
    infoBubble.close();
  });


}
google.maps.event.addDomListener(window, 'load', init);

/*
| ----------------------------------------------------------------------------------
| TABLE OF CONTENT
| ----------------------------------------------------------------------------------

-Preloader
-Select customization
-Range slider
-View catalog
-Map
-Owl carousel
-SCALE IMAGES
*/



$(document).ready(function() {

  "use strict";



// PRELOADER

    var $preloader = $('#page-preloader'),
    $spinner   = $preloader.find('.spinner-loader');
    $spinner.fadeOut();
    $preloader.delay(50).fadeOut('slow');


  // SELECT CUSTOMIZATION

  if ($('.select_box').length > 0) {
    $(".select_box").chosen({
      disable_search_threshold: 10
    });
  }
  $('#a[data-toggle="tab"]').on('shown.bs.tab', function () {
    $('select').chosen({width: '100%'});
  });

  // RANGE SLIDER

  if ($('.range_01').length > 0) {
    $(".range_01").ionRangeSlider({
      min: 0,
      max: 500,
      from: 250
    });
  }

  if ($('.range_02').length > 0) {
    $(".range_02").ionRangeSlider({
      min: 0,
      max: 1000,
      from: 500
    });
  }

  if ($('.range_03').length > 0) {
    $(".range_03").ionRangeSlider({
      min: 10000,
      max: 1000000,
      from: 500000
    });
  }



  ////////////////////////////////////////////
  // VIEW CATALOG
  ///////////////////////////////////////////


  if ($('.btns-switch').length > 0) {
    $('.btns-switch__item').on( 'click', function() {
      $('.btns-switch').find('.active').removeClass('active');
      $( this ).addClass('active');
    });

    $('.js-view-col').on( 'click', function() {
      $('.list-projects').addClass('list-projects_col');
    });

    $('.js-view-list').on( 'click', function() {
      $('.list-projects').removeClass('list-projects_col');
    });
  }



  ////////////////////////////////////////////
  // MAP
  ///////////////////////////////////////////

  if ($('#map-ua').length > 0) {

      jQuery('#map-ua').vectorMap({
        map: 'ukraine',
        showLabels: false,
        backgroundColor: 'transparent',
        borderColor: '#FF9900',
        borderOpacity: 0.60,
        borderWidth: 2,
        color: '#1076C8',
        hoverColor: '#0A4C82',
        selectedColor: '#FF9900',
        enableZoom: true,
        showTooltip: true
        });
  }


  ////////////////////////////////////////////
  // OWL CAROUSEL
  ///////////////////////////////////////////

  if ($('.owl-carousel').length > 0) {
    $(".owl-carousel").owlCarousel({
      center: true,
      items:1,
      loop:true,
      nav:true,
      dots: false,
      animateOut: 'slideOutDown',
      animateIn: 'flipInX',
      smartSpeed:450,
      autoplay: true,
      responsive:{
          600:{
              items:2
          }
      }
    });
  }


  ////////////////////////////////////////////
  // Parallax
  ///////////////////////////////////////////
  $('.jarallax').jarallax({
      speed: 0.3,
      type: 'scroll-opacity',
      zIndex: 0
    });



/////////////////////////////////////////////////////////////////
// SCALE IMAGES
/////////////////////////////////////////////////////////////////

  if ($('.img-scale').length) {
    $(function () { objectFitImages('.img-scale') });
  }

});

